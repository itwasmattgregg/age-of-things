import Vue from "vue";
import Vuex from "vuex";
import createEasyFirestore from "vuex-easy-firestore";
import "./firebase";
import * as firebase from "firebase";

Vue.use(Vuex);

const myModule = {
  firestorePath: "things/{userId}/things",
  firestoreRefType: "collection", // or 'doc'
  moduleName: "myModule",
  statePropName: "things",
  namespaced: true // optional but recommended
  // you can also add your own state/getters/mutations/actions
};

// do the magic 🧙🏻‍♂️
const easyFirestore = createEasyFirestore([myModule], {
  logging: false
});

// open the DB channel

const store = new Vuex.Store({
  plugins: [easyFirestore],
  getters: {
    sortedThings: state => {
      return Object.values(state.myModule.things).sort((a, b) => {
        return b.date.seconds - a.date.seconds;
      });
    }
  }
});

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    // Signed in. Let Vuex Easy Firestore know.
    store.dispatch("myModule/openDBChannel"); // open the DB channels
  } else {
    // Signed out. Let Vuex know.
    store.dispatch("myModule/closeDBChannel", { clearModule: true });
    store.dispatch("myModule/clearUser");
  }
});

export default store;
