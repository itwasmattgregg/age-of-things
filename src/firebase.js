const config = {
  apiKey: "AIzaSyBp23S2UE5fhq5yNLtQqVgDlnEiBloRIPo",
  authDomain: "age-of-things.firebaseapp.com",
  databaseURL: "https://age-of-things.firebaseio.com",
  projectId: "age-of-things",
  storageBucket: "",
  messagingSenderId: "1027003732023"
};

import firebase from "firebase";
import "firebase/firestore";
const firebaseApp = firebase.initializeApp(config);

const firestore = firebaseApp.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

export default firestore;
